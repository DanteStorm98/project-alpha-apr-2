from tasks.views import create_task, show_my_tasks
from django.urls import path


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
